const waitFor = (delay:number) => new Promise(resolve => setTimeout(resolve, delay));

function split_ext (filename:string): string {
  let m = filename.match(/\.([^.]+)$/);
  if (m) {
    return m[1];
  }
  return "";
}

type TotalFilesByExt = {
  [key: string]: {
    count: number,
    bytes: number
  }
}

type UnloadedDirectoryNode = {
  type: "directory",
  filename: string,
  parent?: LoadedDirectoryNode
}

type LoadedDirectoryNode = {
  type: "directory",
  filename: string,
  url: string,
  contents:DirectoryOrFileNode[],
  totalBytesByExt:TotalFilesByExt,
  totalBytes: number,
  parent?: LoadedDirectoryNode
}

type DirectoryNode = UnloadedDirectoryNode | LoadedDirectoryNode;

export type FileNode = {
  type: "file",
  filename: string,
  id: string,
  parent?: LoadedDirectoryNode
}

type DirectoryOrFileNode = DirectoryNode | FileNode;

/*
function url_for (f: DirectoryOrFileNode) {
    let ret = f.id;
    while (f.parent) {
        f = f.parent;
        ret = f.id
    }
}
*/

async function load_tree (base_url: string) {
  let index_url = `${base_url}.index/.index.json`; 
  try {
    let r = await fetch(index_url);
    if (!r.ok) {
      console.log(`problem loading ${index_url}`);
    } else {
      let dir_info = await r.json();
      console.log("d", dir_info);
      for (var i=0; i<dir_info.contents.length; i++) {
        let child_info:DirectoryOrFileNode = dir_info.contents[i];      
        if (child_info.type == "file") {
          let ext = split_ext(child_info.filename).toLowerCase();
          // console.log(`I see a ${ext}`)
          if (ext == "mp3") {
          }
        } else if (child_info.type == "directory") {
          let child_base = `${base_url}${child_info.filename}/`;
          // console.log(child_base);
          // await waitFor(250);
          // await load_tree(child_base);
        }
      };
    }
  } catch (e) {
    console.log(`Exception parsing ${index_url}: ${e}`);
  }
}

function choice<T> (items: T[]): T {
  let rand_item = Math.floor(items.length * Math.random());
  return items[rand_item];
}

function index_for_directory_url (url:string): string {
  return `${url}.index/.index.json`;
}

export async function load_directory_node (url: string, mapper = index_for_directory_url): Promise<LoadedDirectoryNode|undefined> {
  let index_url = mapper(url);

  try {
    let r = await fetch(index_url);
    if (!r.ok) {
      console.log(`load_directory_node: problem loading ${index_url}`);
    } else {
      let ret:LoadedDirectoryNode = await r.json();
      ret.url = url;
      // connect file nodes parents to this
      for (var i=0; i<ret.contents.length; i++) {
        ret.contents[i].parent = ret;
      }
      return ret;
    }
  } catch (e) {
    console.log(`load_directory_node: Exception parsing ${index_url}: ${e}`);
  }
  return;
}

export async function random_pick (d: LoadedDirectoryNode) : Promise<FileNode | undefined> {
  // what nodes are possible
  let possible:DirectoryOrFileNode[] = [];
  for (let i=0; i<d.contents.length; i++) {
    let cn = d.contents[i];
    if (cn.type == "file") {
      let ext = split_ext(cn.filename).toLowerCase();
      if (ext == "mp3" || ext == "ogg") {
        possible.push(cn);
      }
    } else if (cn.type == "directory") {
      if ("contents" in cn) {
        // check if the loaded directory node has oggs
        if ("ogg" in cn.totalBytesByExt && cn.totalBytesByExt["ogg"].count > 0) {
          possible.push(cn);
        }
      } else {
        // unloaded directory nodes are possible
        possible.push(cn);
      }
    }
  }
  while (possible.length > 0) {
    console.log(`picking from ${possible.length} possible nodes`)
    let rc = choice(possible);
    if (rc.type == "file") {
      // rc.url = `${d.url}${rc.id}`;
      return rc;
    } else if (rc.type == "directory") {
      if ("contents" in rc) {
        console.log("recursing into rc, guaranteed to have audio", rc.totalBytesByExt);
        return await random_pick(rc);
      } else {
        // ensure loaded and update the UnloadedDirectory in place
        let rc_url = `${d.url}${rc.filename}/`; 
        let rc_loaded = await load_directory_node(rc_url);
        if (rc_loaded) {
          for (let i=0; i<d.contents.length; i++) {
            let cn = d.contents[i];
            if (cn == rc) {
              d.contents[i] = rc_loaded;
              break
            }
          }
          let ret = await random_pick(rc_loaded);
          if (!ret) {
            // remove this node from possible
            for (let pi=0; pi<possible.length; pi++) {
                if (possible[pi] == rc) {
                    possible.splice(pi, 1);
                    break;
                }
            }
          } else {
            console.log("returing post load value", ret);
            return ret;
          }
        }
      }  
    }
  }
  console.log("reached end of function, returning undefined");
  return;
}
